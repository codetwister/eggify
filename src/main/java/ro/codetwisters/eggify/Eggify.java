package ro.codetwisters.eggify;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class Eggify {

    private List<Object> triggerList;
    private List<Object> events;
    private Action action;

    // making sure we use the builder
    private Eggify() {
        triggerList = new ArrayList<>();
        events = new ArrayList<>();
    }

    boolean containsTrigger(Object event) {
        return this.triggerList.contains(event);
    }

    public static Builder builder() {
        return new Builder();
    }

    Action getAction() {
        return action;
    }

    @SuppressWarnings("WeakerAccess")
    public void onEvent(Object event) {
        events.add(event);
        if (checkTrigger()) {
            action.doSomethingWhenTriggered();
            events.clear();
        }
    }

    private boolean checkTrigger() {
        int eventIndex = events.size() - 1;
        for (int i = triggerList.size()-1; i > 0; i--) {
            if (eventIndex < 0 || eventIndex >= events.size()) {
                return false;
            }
            if (!triggerList.get(i).equals(events.get(eventIndex))) {
                return false;
            }
            eventIndex--;
        }
        return true;
    }

    boolean eventHappened(Object event) {
        return events.contains(event);
    }

    @SuppressWarnings("WeakerAccess")
    public static class Builder {

        Eggify eggify;

        private Builder() {
            eggify = new Eggify();
        }

        public Eggify build() {
            return eggify;
        }

        public Builder addTrigger(Object event) {
            eggify.triggerList.add(event);
            return this;
        }

        public Builder setAction(Action action) {
            eggify.action = action;
            return this;
        }
    }

    @SuppressWarnings("WeakerAccess")
    public interface Action {
        void doSomethingWhenTriggered();
    }
}
