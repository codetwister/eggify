package ro.codetwisters.eggify;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;

public class EggifyTest {

    private Eggify.Action mockAction;
    private Eggify eggify;

    @Test
    public void testCreateEmptyConfiguration() {
        Eggify eggify = Eggify.builder().build();
        Assert.assertNotNull("Builder should create not null object", eggify);
    }

    @Test
    public void testAddTriggerToBuilderConfiguration() {
        Eggify eggify = Eggify.builder()
            .addTrigger(TestEvent.HEADER_CLICK)
            .build();

        Assert.assertTrue("eggify should contain event if added in builder", eggify.containsTrigger(TestEvent.HEADER_CLICK));
    }

    @Test
    public void testSetActionToBuilderConfiguration() {
        Eggify.Action action = Mockito.mock(Eggify.Action.class);

        Eggify eggify = Eggify.builder()
            .setAction(action)
            .build();

        Assert.assertEquals("action should be set properly when creating eggify", eggify.getAction(), action);
    }

    @Test
    public void testOnEvent() {
        setupTestEggify();

        eggify.onEvent(TestEvent.HEADER_CLICK);

        Assert.assertTrue(eggify.eventHappened(TestEvent.HEADER_CLICK));
    }

    @Test
    public void testActionTriggered() {
        setupTestEggify();

        eggify.onEvent(TestEvent.HEADER_CLICK);
        eggify.onEvent(TestEvent.DETAILS_SCREEN_SHOWN);
        eggify.onEvent(TestEvent.ROW_LONG_TAP);
        eggify.onEvent(TestEvent.SHAKE);

        Mockito.verify(mockAction, VerificationModeFactory.times(1)).doSomethingWhenTriggered();
    }

    @Test
    public void testActionNotTriggeredForInvalidLength() {
        setupTestEggify();

        eggify.onEvent(TestEvent.HEADER_CLICK);
        eggify.onEvent(TestEvent.DETAILS_SCREEN_SHOWN);
        eggify.onEvent(TestEvent.ROW_LONG_TAP);

        Mockito.verify(mockAction, VerificationModeFactory.noMoreInteractions()).doSomethingWhenTriggered();
    }

    @Test
    public void testActionNotTriggeredForInvalidOrder() {
        setupTestEggify();

        eggify.onEvent(TestEvent.SHAKE);
        eggify.onEvent(TestEvent.HEADER_CLICK);
        eggify.onEvent(TestEvent.DETAILS_SCREEN_SHOWN);
        eggify.onEvent(TestEvent.ROW_LONG_TAP);

        Mockito.verify(mockAction, VerificationModeFactory.noMoreInteractions()).doSomethingWhenTriggered();
    }

    @Test
    public void testActionTriggeredWhenOtherEventsPrecede() {
        setupTestEggify();

        eggify.onEvent(TestEvent.DETAILS_SCREEN_SHOWN);
        eggify.onEvent(TestEvent.ROW_LONG_TAP);
        eggify.onEvent(TestEvent.HEADER_CLICK);
        eggify.onEvent(TestEvent.DETAILS_SCREEN_SHOWN);
        eggify.onEvent(TestEvent.ROW_LONG_TAP);
        eggify.onEvent(TestEvent.SHAKE);

        Mockito.verify(mockAction, VerificationModeFactory.times(1)).doSomethingWhenTriggered();
    }

    private void setupTestEggify() {
        mockAction = Mockito.mock(Eggify.Action.class);
        eggify = Eggify.builder()
            .setAction(mockAction)
            .addTrigger(TestEvent.HEADER_CLICK)
            .addTrigger(TestEvent.DETAILS_SCREEN_SHOWN)
            .addTrigger(TestEvent.ROW_LONG_TAP)
            .addTrigger(TestEvent.SHAKE).build();
    }

}
