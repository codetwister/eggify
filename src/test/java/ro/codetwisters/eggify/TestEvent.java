package ro.codetwisters.eggify;

enum TestEvent {
    HEADER_CLICK,
    ROW_LONG_TAP,
    SHAKE,
    DETAILS_SCREEN_SHOWN
}
