Readme
======

Adding Eggify to your project
-----------------------------

Put this in your project gradle file

```
repositories {
    ...
    maven { url = 'https://jitpack.io' }
    ...
}
```

And this to your dependencies
```
dependencies {
    ...
    compile 'org.bitbucket.codetwister:eggify:master-SNAPSHOT'
    ...
}
```

We don't have an official release yet, stay tuned until we do

Quick guide to use Eggify
-------------------------
